﻿using TMPro;
using UnityEngine;
using UnityEngine.Rendering;

public class Gun : MonoBehaviour
{

    public float damage = 10f;
    public float range = 100f;
    public float fireRate = 15f;
    public float bulletForce = 1000f;

    public ParticleSystem muzzleFlash;
    public ParticleSystem bulletTrailSpread;
    public ParticleSystem bulletTrailAccurate;

    public Camera fpsCam;

    public GameObject impactEffect;

    public AudioSource gunShot;

    private float nextTimeToFire = 0f;

    public static bool canFire;

    public static bool isShooting;

    public float bulletCount;
    public float clipSize;

    public TextMeshProUGUI bulletText;


    // Update is called once per frame
    void Update()
    {
        bulletText.SetText(bulletCount + " / " + clipSize);
        if(Input.GetButtonDown("Fire1") && Time.time >= nextTimeToFire && bulletCount > 0)
        {
            bulletCount--;
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
            isShooting = true;
        }
        else
        {
            isShooting = false;
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            Reload();
        }
    }

    void Shoot()
    {
        muzzleFlash.Play();
        if (AimDownSights.isAiming)
        {
            bulletTrailAccurate.Play();
        }
        else
        {
            bulletTrailSpread.Play();
        }
        gunShot.Play(0);
    }

    void Reload()
    {
        bulletCount = clipSize;
    }
}
