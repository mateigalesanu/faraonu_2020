﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AimDownSights : MonoBehaviour
{
    public float maxAngle;


    public float smoothAim;

    public float aimX, aimY, aimZ;

    public Vector3 normalPos;

    public ParticleSystem bullet;

    public static bool isAiming;

    public Animator walkAnim;



    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetPos;

        if (Input.GetButton("Fire2"))
        {
            isAiming = true;
            targetPos = new Vector3(aimX, aimY, aimZ);
        }
        else
        {
            isAiming = false;
            targetPos = new Vector3(0f, 0f, 0f);
        }

        transform.localPosition = Vector3.Lerp(transform.localPosition, targetPos + normalPos, Time.deltaTime * smoothAim);

    }
}
