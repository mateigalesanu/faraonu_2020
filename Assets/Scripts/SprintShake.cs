﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SprintShake : MonoBehaviour
{

    public float smoothSprint;
    public float sprintX, sprintY, sprintZ;
    public float sprintStep = 5;
    private Vector3 initialPos;
    private Quaternion initialRot;

    public Transform pistol;
    
    private Vector3 sprintV;
    public Quaternion sprintQ;

    public float smoothT = 0f;
    public float smoothClamp;
    public float smoothPlus;

    public static bool sprint;


    // Start is called before the first frame update
    void Start()
    {
        initialPos = pistol.localPosition;
        initialRot = pistol.localRotation;
    }

    // Update is called once per frame
    void Update()
    {       
        
        if (Input.GetKey(KeyCode.LeftShift) && sprint)
        {
            GetComponent<Recoil>().enabled = false;
            sprintV = new Vector3(sprintX, sprintY, sprintZ);
            smoothT += smoothPlus;
        }
        else
        {
            sprintV = new Vector3(0f, 0f, 0f);
            smoothT -= 2*smoothPlus;
        }

        smoothT = Mathf.Clamp(smoothT, 0, smoothClamp);


        pistol.localPosition = Vector3.Lerp(pistol.localPosition, sprintV + initialPos, Time.deltaTime * smoothSprint);
        pistol.localRotation = Quaternion.Lerp(initialRot, sprintQ, smoothT);

    }


}
