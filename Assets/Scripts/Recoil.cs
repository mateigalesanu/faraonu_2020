﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Recoil : MonoBehaviour
{

    //Recoil
    public float recoilKickback;
    public float recoilStrength;
    private float currentRecoil;
    public float smoothRecoil;
    public static bool applyRecoil;
    private Quaternion initialRot;

    public Transform pistol;
    // Start is called before the first frame update
    void Start()
    {
        initialRot = pistol.localRotation;
        applyRecoil = false;
        currentRecoil = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Fire();
    }

    void Fire()
    {
        if (applyRecoil)
        {
            currentRecoil += 4*recoilStrength;
        }
        else
        {
            currentRecoil -= recoilStrength;
        }


        if (currentRecoil < 0)
            currentRecoil = 0;

        if (currentRecoil >= 1)
            applyRecoil = false;

        Quaternion finalRotation = Quaternion.Euler(-35, 0, 0);
        pistol.localRotation = Quaternion.Lerp(initialRot, finalRotation, currentRecoil);
    }

}
