﻿using UnityEngine;
using System.Collections;

public class CharacterControls : MonoBehaviour
{

	public float speed = 10.0f;
	public float gravity = 10.0f;
	public float maxVelocityChange = 10.0f;
	public bool canJump = true;
	public float jumpHeight = 2.0f;
	private bool grounded = false;
	public Rigidbody rb;

	public static float shiftMultiplier = 2f;

	public Animator gunWalkAnimator;


	void Awake()
	{
		rb.freezeRotation = true;
		rb.useGravity = false;
	}

	void FixedUpdate()
	{
		if (grounded)
		{
			float moveH = Input.GetAxis("Horizontal");
			float moveV = Input.GetAxis("Vertical");


			if (moveH != 0 || moveV != 0)
			{
				gunWalkAnimator.SetBool("isWalking", true);
				gunWalkAnimator.enabled = true;
			}
			else
			{
				gunWalkAnimator.SetBool("isWalking", false);
				gunWalkAnimator.enabled = false;
			}

			// Calculate how fast we should be moving
			Vector3 targetVelocity = new Vector3(moveH, 0, moveV);
			targetVelocity = transform.TransformDirection(targetVelocity);
			if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.W) && SprintShake.sprint)
			{
				shiftMultiplier = 2f;
				gunWalkAnimator.speed = 1.3f;
			}
			else
			{
				shiftMultiplier = 1f;
				gunWalkAnimator.speed = 1f;
			}
			targetVelocity *= speed * shiftMultiplier;

			// Apply a force that attempts to reach our target velocity
			Vector3 velocity = rb.velocity;
			Vector3 velocityChange = (targetVelocity - velocity);
			velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
			velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
			velocityChange.y = 0;
			rb.AddForce(velocityChange, ForceMode.VelocityChange);

			// Jump
			if (canJump && Input.GetButton("Jump"))
			{
				rb.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
			}
		}

		// We apply gravity manually for more tuning control
		rb.AddForce(new Vector3(0, -gravity * rb.mass, 0));

		grounded = false;
	}

	void OnCollisionStay()
	{
		grounded = true;
	}

	float CalculateJumpVerticalSpeed()
	{
		// From the jump height and gravity we deduce the upwards speed 
		// for the character to reach at the apex.
		return Mathf.Sqrt(2 * jumpHeight * gravity);
	}
}