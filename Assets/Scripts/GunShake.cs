﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class GunShake : MonoBehaviour
{

    public float amount;
    public float maxAmount;
    public float smoothAmount;

    public float smooth1;
    public float smoothMultiplier;

    public Transform pistol;

    public Rigidbody player;

    public float xClamp = 0.2f, yClamp, zClamp = 0.1f;

    public Vector3 normalPos;
    public Vector3 recoilBack;

    void Start()
    {

    }

    void Update()
    {
        float movementX = -Input.GetAxis("Mouse X") * amount;
        float movementY = -Input.GetAxis("Mouse Y") * amount;


        Vector3 finalPosition = new Vector3(movementX, movementY, 0);


        Vector3 opposeMovement = transform.InverseTransformDirection(-player.velocity);
        opposeMovement.x = Mathf.Clamp(opposeMovement.x, -xClamp * CharacterControls.shiftMultiplier, xClamp * CharacterControls.shiftMultiplier);
        opposeMovement.y = Mathf.Clamp(opposeMovement.y, -yClamp, yClamp);
        opposeMovement.z = Mathf.Clamp(opposeMovement.z, -zClamp * CharacterControls.shiftMultiplier, zClamp * CharacterControls.shiftMultiplier);

        opposeMovement.x = 0;
        opposeMovement.z = 0;
        Vector3 targetPos;
        if (Recoil.applyRecoil)
        {
            targetPos = finalPosition + opposeMovement + recoilBack + normalPos;
            smoothAmount = 5*smooth1;
        }
        else
        {
            targetPos = finalPosition + opposeMovement + normalPos;
            smoothAmount = smooth1;
        }
        
        transform.localPosition = Vector3.Lerp(transform.localPosition, targetPos, Time.deltaTime * smoothAmount);

    }

}
