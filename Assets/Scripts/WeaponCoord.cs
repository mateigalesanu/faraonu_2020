﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponCoord : MonoBehaviour
{
    public Animator walkAnimator;
    public Image crosshair;

    // Update is called once per frame
    void Update()
    {
        if(Gun.isShooting)
        {
            gameObject.GetComponentInChildren<Recoil>().enabled = true;
            Recoil.applyRecoil = true;
            SprintShake.sprint = false;
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            SprintShake.sprint = true;
        }

        if(AimDownSights.isAiming)
        {
            crosshair.enabled = false;
            gameObject.GetComponentInChildren<GunShake>().enabled = false;
            SprintShake.sprint = false;
            walkAnimator.enabled = false;
        }
        else
        {
            crosshair.enabled = true;
            gameObject.GetComponentInChildren<GunShake>().enabled = true;
        }
    }
}
